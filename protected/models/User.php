<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $role
 * @property string $email
 * @property string $password
 * @property string $last_login
 * @property string $registered_on
 * @property string $name
 * @property string $phone
 * @property integer $country_id
 * @property string $discount_code
 *
 * The followings are the available model relations:
 * @property Property[] $properties
 * @property Property[] $properties1
 * @property Country $country
 */
class User extends CActiveRecord {
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('role, email, password, last_login, registered_on, name, phone, country_id, discount_code', 'required'),
            array('role, country_id', 'numerical', 'integerOnly' => true),
            array('email, password, name, phone', 'length', 'max' => 255),
            array('discount_code', 'length', 'max' => 12),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, role, email, password, last_login, registered_on, name, phone, country_id, discount_code', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'properties' => array(self::HAS_MANY, 'Property', 'edited_by'),
            'properties1' => array(self::HAS_MANY, 'Property', 'user_id'),
            'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'role' => 'Role',
            'email' => 'Email',
            'password' => 'Password',
            'last_login' => 'Last Login',
            'registered_on' => 'Registered On',
            'name' => 'Name',
            'phone' => 'Phone',
            'country_id' => 'Country',
            'discount_code' => 'Discount Code',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('email', $this->filter, true, 'OR');
        $criteria->compare('name', $this->filter, true, 'OR');

        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    public function is($role) {
        return Role::is($this->role, $role);
    }

    public function changePassword() {
        if ($this->password !== $this->password2)
            $this->addError('password', 'New password doesn\'t match');
        else {
            $this->saveAttributes(array(
                'password' => sha1($this->password),
            ));
            if ($this->passwordReset) {
                $this->passwordReset->delete();
                return true;
            }
        }
    }
}
