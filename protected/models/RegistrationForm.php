<?php

use \application\utils\Event as event;

class RegistrationForm extends CFormModel {
	public $email;
	public $password;
	public $name;
	public $origin;

	private $_identity;

	public function init() {
		event::callback('onRegister', $this, 'sendRegistrationEmail');
	}

	public function rules() {
		return array(
			array('email, password, name', 'required'),
			array('email', 'email'),
		);
	}

	public function register() {
		if (!User::model()->exists('email = :email', array(':email' => $this->email))) {
			$user = new User();
			$user->attributes = $this->attributes;
			$user->password = sha1($this->password);
			$user->registered_on = $user->last_login = date(DATE_ISO8601);
			$user->country_id = Country::model()->default->id;
			$user->origin = $this->getOriginFromReferrerUrl();
			$user->role = Role::USER;

			return $user->save();
		}
		return true;
	}

	public function getOriginFromReferrerUrl() {
		$referrer = Yii::app()->request->urlReferrer;
		if (strpos($referrer, 'onlineflexbvoprichten.nl') !== false)
			return User::ORIGIN_ONLINEFLEX;
		return User::ORIGIN_FIRM24;
	}

	public function sendRegistrationEmail(CEvent $e) {
		$user = $e->sender;

		$mail = new YiiMailer();
		$mail->IsSMTP();
		$mail->setTo(array($user->email => $user->name));
		$mail->setSubject('Uw gegevens bij Firm24');
		$mail->setView('registration');
		$mail->setData(array('user' => $user, 'title' => 'Uw gegevens bij Firm24'));
		$mail->send();
	}
}
