<?php

/**
 * This is the model class for table "property".
 *
 * The followings are the available columns in table 'property':
 * @property integer $id
 * @property string $name
 * @property integer $property_type_id
 * @property integer $property_category_id
 * @property integer $area_id
 * @property integer $user_id
 * @property string $lon_lat
 * @property string $address
 * @property string $phone
 * @property string $phone_alt
 * @property string $phone_alt_2
 * @property string $fax
 * @property string $email
 * @property string $site_url
 * @property string $social_links
 * @property string $description
 * @property integer $no_of_rooms
 * @property string $last_edit
 * @property integer $edited_by
 *
 * The followings are the available model relations:
 * @property User $editedBy
 * @property PropertyType $propertyType
 * @property PropertyCategory $propertyCategory
 * @property Area $area
 * @property User $user
 * @property Amenity[] $amenities
 * @property PropertyExtra[] $propertyExtras
 * @property PropertyPeriod[] $propertyPeriods
 * @property PropertySeo $propertySeo
 */
class Property extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'property';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, property_type_id, property_category_id, last_edit, edited_by', 'required'),
			array('property_type_id, property_category_id, area_id, user_id, no_of_rooms, edited_by', 'numerical', 'integerOnly'=>true),
			array('name, lon_lat, address, phone, phone_alt, phone_alt_2, fax, email, site_url', 'length', 'max'=>255),
			array('social_links, description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, property_type_id, property_category_id, area_id, user_id, lon_lat, address, phone, phone_alt, phone_alt_2, fax, email, site_url, social_links, description, no_of_rooms, last_edit, edited_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'editedBy' => array(self::BELONGS_TO, 'User', 'edited_by'),
			'propertyType' => array(self::BELONGS_TO, 'PropertyType', 'property_type_id'),
			'propertyCategory' => array(self::BELONGS_TO, 'PropertyCategory', 'property_category_id'),
			'area' => array(self::BELONGS_TO, 'Area', 'area_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'amenities' => array(self::MANY_MANY, 'Amenity', 'property_amenity(property_id, amenity_id)'),
			'propertyExtras' => array(self::HAS_MANY, 'PropertyExtra', 'property_id'),
			'propertyPeriods' => array(self::HAS_MANY, 'PropertyPeriod', 'property_id'),
			'propertySeo' => array(self::HAS_ONE, 'PropertySeo', 'property_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'property_type_id' => 'Property Type',
			'property_category_id' => 'Property Category',
			'area_id' => 'Area',
			'user_id' => 'User',
			'lon_lat' => 'Lon Lat',
			'address' => 'Address',
			'phone' => 'Phone',
			'phone_alt' => 'Phone Alt',
			'phone_alt_2' => 'Phone Alt 2',
			'fax' => 'Fax',
			'email' => 'Email',
			'site_url' => 'Site Url',
			'social_links' => 'Social Links',
			'description' => 'Description',
			'no_of_rooms' => 'No Of Rooms',
			'last_edit' => 'Last Edit',
			'edited_by' => 'Edited By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('property_type_id',$this->property_type_id);
		$criteria->compare('property_category_id',$this->property_category_id);
		$criteria->compare('area_id',$this->area_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('lon_lat',$this->lon_lat,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('phone_alt',$this->phone_alt,true);
		$criteria->compare('phone_alt_2',$this->phone_alt_2,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('site_url',$this->site_url,true);
		$criteria->compare('social_links',$this->social_links,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('no_of_rooms',$this->no_of_rooms);
		$criteria->compare('last_edit',$this->last_edit,true);
		$criteria->compare('edited_by',$this->edited_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Property the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
