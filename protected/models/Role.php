<?php

/**
 * Description of Role
 *
 * @author kthermos
 */
class Role {

	const USER = 1;
	const ADMIN = 16;

	public static function is($userRole, $role) {
		return $role & $userRole;
	}

	public static function getRoles() {
		return array(
			self::USER => 'User',
			self::ADMIN => 'Admin',
		);
	}

	public static function toText($role) {
		switch ($role) {
			case self::USER:
				return 'User';
			case self::ADMIN:
				return 'Admin';
			default:
				return false;
				break;
		}
	}

}