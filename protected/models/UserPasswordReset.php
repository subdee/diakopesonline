<?php

use \application\utils\Event as event;
use \application\utils\Url as url;

/**
 * This is the model class for table "user_password_reset".
 *
 * The followings are the available columns in table 'user_password_reset':
 * @property integer $user_id
 * @property string $code
 * @property string $requested_on
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserPasswordReset extends CActiveRecord {

	public function init() {
		event::callback('onPasswordReset', $this, 'sendPasswordRequestEmail');
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'user_password_reset';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code, requested_on', 'required'),
			array('code', 'length', 'max' => 32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, code, requested_on', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'user_id' => 'User',
			'code' => 'Code',
			'requested_on' => 'Requested On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('code', $this->code, true);
		$criteria->compare('requested_on', $this->requested_on, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserPasswordReset the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function beforeValidate() {
		$this->code = $this->_createCode();
		$this->requested_on = date(DATE_ISO8601);

		return parent::beforeValidate();
	}

	public function verify($code) {
		return $this->find('code = :code AND requested_on >= NOW() - INTERVAL 1 DAY', array(':code' => $code));
	}

	public function sendPasswordRequestEmail(CEvent $e) {
		$user = $e->sender;
		$user->refresh();

		$mail = new YiiMailer();
		$mail->IsSMTP();
		$mail->setTo(array($user->email => $user->name));
		$mail->setSubject('Nieuw wachtwoord aanvraag');
		$mail->setView('password-reset');
		$mail->setData(array(
			'user' => $user,
			'link' => url::createAbsolute('dashboard/reset', array('code' => $user->passwordReset->code)),
			'title' => 'Nieuw wachtwoord aanvraag'
		));
		$mail->send();
	}

	private function _createCode() {
		return md5(time() . $this->user_id);
	}
}
