<?php
namespace Codeception\Module;

// here you can define custom functions for WebGuy 

class WebHelper extends \Codeception\Module {
	public function seeDownloadIsValid($elem) {
		$session = $this->getModule('Selenium2')->session;
		$link = $session->getPage()->find('css', $elem);
		$url = $link->getAttribute('href');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_setopt($ch, CURLOPT_HEADER, true);
		$response = curl_exec($ch);
		$error = curl_error($ch);
		curl_close($ch);
		if (preg_match('/HTTP\/1\.\d+\s+(\d+)/', $response, $matches))
			$code = intval($matches[1]);
		else
			$this->assertTrue(false, 'Call to URL failed. Error is: ' . $error . ' -- URL was: ' . $url);
		$this->assertTrue($code < 400, 'HTTP code was: ' . $code . ' -- URL was: ' . $url);
	}
}
