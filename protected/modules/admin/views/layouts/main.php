<?php
/* @var $this Controller */
use application\utils\Url as url;
use application\utils\File as file;
use application\utils\Profile as profile;
use application\utils\Module as module;

?>
<?php $this->beginContent('//layouts/site'); ?>
<?php module::instance($this->module)->css('main'); ?>
<div class="container">
    <?php echo $content; ?>
</div>
<?php $this->endContent(); ?>