<?php /* @var $property Property */ ?>
<?php use \application\utils\Url as url; ?>
<div class="nav navbar navbar-fixed-top">
	<a class="btn" href="#type">Type</a>
	<a class="btn" href="#location">Location</a>
	<a class="btn" href="#info">Info</a>
	<a class="btn" href="#amenities">Amenities</a>
	<a class="btn" href="#periods">Periods</a>
	<a class="btn" href="#images">Images</a>
</div>
<hr>
<div class="row form">
	<div class="span12">
		<?php
		/* @var $form TbActiveForm */
		$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
			'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
			'htmlOptions' => ['enctype' => 'multipart/form-data']
		]); ?>

		<?php $this->widget('Notification', ['models' => [$property, $propertyExtra, $propertySeo, $propertyPeriod, $propertyAmenity]]); ?>
	</div>
</div>
<div class="row">
	<a name="type" class="links">&nbsp;</a>
	<h4>Type</h4>

	<div class="span12">
		<?php echo $form->textFieldControlGroup($property, 'name'); ?>
		<div class="control-group">
			<div class="control-label"><?php echo TbHtml::label('Custom info', ''); ?></div>
			<div class="control">
				<?php echo $form->uneditableField($propertyExtra, 'title'); ?>
				<?php echo $form->textField($propertyExtra, 'value'); ?>
			</div>
		</div>
		<?php echo $form->dropDownListControlGroup($property, 'property_type_id', TbHtml::listData(PropertyType::model()->findAll(), 'id', 'name'), ['prompt' => 'Select']); ?>
		<?php echo $form->dropDownListControlGroup($property, 'property_category_id', TbHtml::listData(PropertyCategory::model()->findAll(), 'id', 'category'), ['prompt' => 'Select']); ?>
	</div>
</div>
<hr>
<div class="row">
	<a name="location" class="links">&nbsp;</a>
	<h4>Location</h4>

	<div class="span6">
		<?php echo TbHtml::dropDownListControlGroup('prefecture', '', TbHtml::listData(Prefecture::model()->findAll(), 'id', 'name'), ['ajax' => [
			'url' => url::create('admin/default/areasByPrefecture'),
			'replace' => '#Property_area_id',
		], 'label' => 'Prefecture', 'prompt' => 'Select']); ?>
		<?php echo $form->dropDownListControlGroup($property, 'area_id', TbHtml::listData(Area::model()->with('city')->findAll(), 'id', 'name', 'city.name'), ['prompt' => 'Select']); ?>
		<?php echo $form->textFieldControlGroup($property, 'address'); ?>
		<?php echo $form->hiddenField($property, 'lon_lat'); ?>
		<div class="control-group">
			<div class="control-label"><?php echo TbHtml::label('Define location', ''); ?></div>
			<div class="control">
				<div id="map" class="google-maps"><?php $gmap->renderMap([], Yii::app()->language); ?></div>
			</div>
		</div>
	</div>
	<div class="span6">
		<?php echo $form->textFieldControlGroup($property, 'phone'); ?>
		<?php echo $form->textFieldControlGroup($property, 'phone_alt'); ?>
		<?php echo $form->textFieldControlGroup($property, 'phone_alt_2'); ?>
		<?php echo $form->textFieldControlGroup($property, 'fax'); ?>
		<?php echo $form->textFieldControlGroup($property, 'email'); ?>
		<?php echo $form->textFieldControlGroup($property, 'site_url'); ?>
		<?php echo $form->textFieldControlGroup($property, 'social_links'); ?>
	</div>
</div>
<hr>
<div class="row">
	<a name="info" class="links">&nbsp;</a>
	<h4>Info</h4>

	<div class="span12">
		<?php echo $form->textAreaControlGroup($property, 'description'); ?>
		<?php echo $form->textFieldControlGroup($property, 'no_of_rooms'); ?>
	</div>
</div>
<hr>
<div class="row">
	<a name="seo" class="links">&nbsp;</a>
	<h4>SEO</h4>

	<div class="span12">
		<?php echo $form->textAreaControlGroup($propertySeo, 'description'); ?>
		<?php echo $form->textAreaControlGroup($propertySeo, 'keywords'); ?>
	</div>
</div>
<hr>
<div class="row">
	<a name="amenities" class="links">&nbsp;</a>
	<h4>Amenities</h4>
	<?php foreach ($amenityCategories as $category) : ?>
		<div class="span1" style="width: 140px;">
			<?php echo $form->checkBoxListControlGroup($propertyAmenity, '[]amenity_id', CHtml::listData($category->amenities, 'id', 'name'), [
				'label' => $category->name,
				'labelOptions' => ['style' => 'font-size: 0.8em; font-weight: bold;'],
			]); ?>
		</div>
	<?php endforeach; ?>
</div>
<hr>
<div class="row">
	<a name="periods" class="links">&nbsp;</a>
	<h4>Periods</h4>

	<div class="span12">
		<?php for ($i = 0; $i < 3; ++$i) : ?>
			<div class="control-group" style="float:left; margin-right: 40px;">
				<?php echo $form->label($propertyPeriod, 'from'); ?>
				<div class="controls">
					<?php echo $form->dropDownList($propertyPeriod, "[{$i}]from_date",
						[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
						['span' => 1, 'prompt' => 'Select date']); ?>
					<?php echo $form->dropDownList($propertyPeriod, "[{$i}]from_month",
						Yii::app()->locale->getMonthNames(), ['span' => 2, 'prompt' => 'Select month']); ?>
				</div>
			</div>
			<div class="control-group">
				<?php echo $form->label($propertyPeriod, 'to'); ?>
				<div class="controls">
					<?php echo $form->dropDownList($propertyPeriod, "[{$i}]to_date",
						[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
						['span' => 1, 'prompt' => 'Select date']); ?>
					<?php echo $form->dropDownList($propertyPeriod, "[{$i}]to_month",
						Yii::app()->locale->getMonthNames(), ['span' => 2, 'prompt' => 'Select month']); ?>
				</div>
			</div>
		<?php endfor; ?>
	</div>
</div>
<hr>
<div class="row">
	<a name="imags" class="links">&nbsp;</a>
	<h4>Images</h4>

	<div class="span12">
		<div>
			<?php echo TbHtml::label('Main image', 'main_image'); ?>
			<?php echo TbHtml::fileField('main_image', '', ['data-url' => url::create('admin/default/upload', array('main' => 1))]); ?>
			<?php echo TbHtml::hiddenField('main_image_x'); ?>
			<?php echo TbHtml::hiddenField('main_image_y'); ?>
			<?php echo TbHtml::hiddenField('main_image_w'); ?>
			<?php echo TbHtml::hiddenField('main_image_h'); ?>
		</div>
		<div>
			<?php echo TbHtml::label('Other images', 'images'); ?>
			<?php echo TbHtml::fileField('images[]', '', ['multiple' => 'multiple', 'data-url' => url::create('admin/default/upload')]); ?>
		</div>
		<div id="image_list"></div>
	</div>
</div>
<hr>
<div>
	<?php echo TbHtml::submitButton('Save', array('color' => TbHtml::BUTTON_COLOR_INFO)); ?>
</div>

<?php $this->endWidget(); ?>
</div>
</div>
<script>
	$(function () {
		$("#Property_address").blur(function () {
			$.ajax({
				url: '<?php echo url::create('admin/default/getAreaInfo'); ?>?id=' + $("#Property_area_id").val() + '&address=' + $(this).val(),
				success: function (data) {
					latLng = new google.maps.LatLng(data.lat, data.long);
					<?php echo $gmap->getJsName(); ?>.
					setCenter(latLng);
					<?php echo $gmap->getJsName(); ?>.
					setZoom(14);
				}
			})
		});
		$('#images').fileupload({
			dataType: 'json',
			done: function (e, data) {
				$("#image_list").append("OK");
			}
		});
		$('#main_image').fileupload({
			dataType: 'json',
			done: function (e, data) {
				var $img = $("<img src='" + data.result.image + "' />");
				$("#image_list").append($img);
				$img.Jcrop({
					minSize: [640,390],
					maxSize: [640,390],
					onSelect: function (c) {
						$("#main_image_x").val(c.x);
						$("#main_image_y").val(c.y);
						$("#main_image_w").val(c.w);
						$("#main_image_h").val(c.h);
					}
				});
			}
		});
	})
</script>