<?php
/* @var $this DashboardController */
/* @var $model LoginForm */
/* @var $form TbActiveForm */

?>

<div class="row">
    <div class="offset4 span4">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'layout' => TbHtml::FORM_LAYOUT_INLINE,
        )); ?>
        <div class="form">
            <?php echo $form->textFieldControlGroup($model, 'email'); ?>
            <br><br>
            <?php echo $form->passwordFieldControlGroup($model, 'password'); ?>
            <br><br>
            <?php
            echo TbHtml::submitButton("Login");
            ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>