<?php

use \application\utils\Url as url;
use \application\utils\Profile as profile;
use \application\utils\File as file;
use \application\utils\Format as format;

class DefaultController extends Controller {

	public $layout = 'main';

	public function filters() {
		return array(
			'accessControl',
		);
	}

	public function accessRules() {
		return array(
			array('allow',
				'actions' => ['login'],
				'users' => ['*'],
			),
			array('allow',
				'expression' => '\\application\\utils\\Profile::isLoggedIn() && \\application\\utils\\Profile::get()->is(Role::ADMIN)',
				'redirect' => ['admin/default/login'],
			),
			array('deny',
				'users' => ['*'],
				'redirect' => ['admin/default/login'],
			),
		);
	}

	public function actionIndex() {
		Yii::app()->clientScript->registerCoreScript('jquery.ui');
		file::js('jquery.fileupload');
		file::js('jquery.Jcrop.min');
		file::css('jquery.Jcrop.min');

		$property = new Property();
		$propertyExtra = new PropertyExtra();
		$propertyExtra->title = 'ΜΗ.Τ.Ε';
		$propertyPeriod = new PropertyPeriod();
		$propertySeo = new PropertySeo();
		$propertyAmenity = new PropertyAmenity();

		if (isset($_POST['Property'])) {
			$t = Yii::app()->db->beginTransaction();
			try {
				$property->attributes = $_POST['Property'];
				$property->edited_by = profile::get()->id;
				$property->last_edit = date(DATE_ISO8601);
				if (!$property->save())
					throw new CException(var_export($property->errors, true));
				if ($_POST['PropertyExtra']['value'] != '') {
					$propertyExtra->attributes = $_POST['PropertyExtra'];
					$propertyExtra->property_id = $property->id;
					if (!$propertyExtra->save())
						throw new CException(var_export($propertyExtra->errors, true));
				}
				if ($_POST['PropertySeo']['keywords'] != '' || $_POST['PropertySeo']['description'] != '') {
					$propertySeo->attributes = $_POST['PropertySeo'];
					$propertySeo->property_id = $property->id;
					if (!$propertySeo->save())
						throw new CException(var_export($propertySeo->errors, true));
				}
				foreach ($_POST['PropertyAmenity'] as $amenity) {
					if (is_array($amenity['amenity_id'])) {
						$amen = new PropertyAmenity();
						$amen->amenity_id = $amenity['amenity_id'][0];
						$amen->property_id = $property->id;
						if (!$amen->save())
							throw new CException(var_export($amen->errors, true));
					}
				}

				foreach ($_POST['PropertyPeriod'] as $pp) {
					if ($pp['from_date'] != '') {
						$propertyPeriod = new PropertyPeriod();
						$propertyPeriod->attributes = $pp;
						$propertyPeriod->property_id = $property->id;
						if (!$propertyPeriod->save())
							throw new CException(var_export($propertyPeriod->errors, true));
					}
				}

				$images = profile::session('images');
				if ($images && is_array($images)) {
					$mainImage = $images['main'];
					if (!$mainImage)
						throw new CException('No main image');

					Yii::import('vendors.WideImage.WideImage');
					$image = WideImage::load(Yii::app()->basePath . '/../upload/' . $mainImage);
					$image = $image->crop($_POST['main_image_x'], $_POST['main_image_y'], $_POST['main_image_w'], $_POST['main_image_h']);
					if (!file_exists(Yii::app()->basePath . '/../images/properties/' . $property->id))
						mkdir(Yii::app()->basePath . '/../images/properties/' . $property->id);
					$image->saveToFile(Yii::app()->basePath . '/../images/properties/' . $property->id .
						'/' . format::seo($property->name) . '_main.' . pathinfo($mainImage, PATHINFO_EXTENSION));
					unlink(Yii::app()->basePath . '/../upload/' . $mainImage);
					unset($images['main']);
					$c = 1;
					foreach ($images as $image) {
						copy(Yii::app()->basePath . '/../upload/' . $image,
							Yii::app()->basePath . '/../images/properties/' . $property->id . '/'
							. format::seo($property->name) . '_' . $c . '.' . pathinfo($image, PATHINFO_EXTENSION));
						unlink(Yii::app()->basePath . '/../upload/' . $image);
						++$c;
					}
				}

				$t->commit();
				profile::session('images', null, true);
				$this->redirect(url::create('admin/default/index'));
			} catch (CException $e) {
				var_dump($e->getMessage());
				$t->rollback();
			} catch (CDbException $e) {
				var_dump($e->getMessage());
				$t->rollback();
			} catch (WideImage_Exception $e) {
				var_dump($e->getMessage());
				$t->rollback();
			}
		}

		$this->render('index', [
			'property' => $property,
			'propertyExtra' => $propertyExtra,
			'propertyPeriod' => $propertyPeriod,
			'propertySeo' => $propertySeo,
			'propertyAmenity' => $propertyAmenity,
			'gmap' => $this->map(),
			'amenityCategories' => AmenityCategory::model()->findAll()
		]);
	}

	public function actionLogin() {
		$model = new LoginForm();
		$model->role = Role::ADMIN;

		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate() && $model->login())
				$this->redirect(url::create('admin/default/index'));
		}
		$this->render('login', ['model' => $model]);
	}

	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(url::create('admin/default/login'));
	}

	public function actionGetAreaInfo($id, $address) {
		if ($area = Area::model()->findByPk($id)) {
			Yii::import('ext.gmap.*');
			$gMap = new EGMap();
			$address = new EGMapGeocodedAddress($address . ',' . $area->name . ',' . $area->city->name . ',' . $area->city->prefecture->name . ',' . $area->city->prefecture->country->name);
			$address->geocode($gMap->getGMapClient());

			header('Content-type: application/json');
			echo json_encode(['lat' => $address->getLat(), 'long' => $address->getLng()]);
		}
		Yii::app()->end();
	}

	public function actionUpload($main = false) {
		if ($main)
			$file = CUploadedFile::getInstanceByName('main_image');
		else
			$file = CUploadedFile::getInstancesByName('images')[0];
		$filename = md5(rand()) . '.' . $file->extensionName;
		$file->saveAs(Yii::app()->basePath . '/../upload/' . $filename);

		if (!$main) {
			Yii::import('vendors.WideImage.WideImage');
			$image = WideImage::load(Yii::app()->basePath . '/../upload/' . $filename);
			$image = $image->resize(150, 150);
			$image->saveToFile(Yii::app()->basePath . '/../upload/' . $filename);
		}

		$images = profile::session('images');
		if (!is_array($images))
			$images = [];
		if ($main)
			$images['main'] = $filename;
		else
			$images[] = $filename;
		profile::session('images', $images);

		header('Content-type: application/json');
		echo json_encode(['image' => Yii::app()->baseUrl . '/upload/' . $filename]);
	}

	private function map($area = null) {
		Yii::import('ext.gmap.*');
		$gMap = new EGMap();
		$gMap->setWidth(300);
		$gMap->setHeight(300);
		$gMap->zoom = 2;
		$gMap->setCenter(5, 33);

		$icon = new EGMapMarkerImage("http://www.google.com/intl/en_us/mapfiles/ms/icons/red-dot.png");
		$icon->setSize(32, 37);
		$icon->setAnchor(16, 16.5);
		$icon->setOrigin(0, 0);

		$dragevent = new EGMapEvent('dragend', 'function (event) {$("#Property_lon_lat").val(event.latLng.lat() + "," + event.latLng.lng());}', false, EGMapEvent::TYPE_EVENT_DEFAULT);

		$gMap->addEvent(new EGMapEvent('click',
			'function (event) {
				var marker = new google.maps.Marker({
					position: event.latLng,
					map: ' . $gMap->getJsName() . ',
                    draggable: true,
                    icon: ' . $icon->toJs() .
			'});' .
			$gMap->getJsName() . '.setCenter(event.latLng);
                var dragevent = ' . $dragevent->toJs('marker') . ';
            }',
			false, EGMapEvent::TYPE_EVENT_DEFAULT));

		return $gMap;
	}
}