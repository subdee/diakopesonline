-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 17, 2013 at 02:21 PM
-- Server version: 5.5.34-0ubuntu0.13.10.1
-- PHP Version: 5.5.3-1ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `diakopesonline`
--

-- --------------------------------------------------------

--
-- Table structure for table `amenity`
--

CREATE TABLE IF NOT EXISTS `amenity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amenity_category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `amenity_category_id` (`amenity_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `amenity`
--

INSERT INTO `amenity` (`id`, `amenity_category_id`, `name`) VALUES
(1, 1, 'Swimming pool'),
(4, 1, 'Garden'),
(5, 1, 'Barbeque'),
(6, 1, 'Patio'),
(7, 1, 'Mountain view'),
(8, 1, 'Sea view'),
(9, 1, 'Patio furniture'),
(10, 1, 'Balcony'),
(11, 1, 'Veranda'),
(12, 2, 'Gym'),
(13, 2, 'Sports rooms'),
(14, 2, 'Marine sports'),
(15, 2, 'Saouna'),
(16, 2, 'Jacuzzi'),
(17, 2, 'Spa'),
(18, 2, 'Hairdresser'),
(19, 2, 'Hydromassage'),
(20, 2, 'Bike rental'),
(21, 2, 'Car rental'),
(22, 2, 'Trips'),
(23, 2, 'Game room'),
(24, 2, 'Massage'),
(25, 2, 'Playground'),
(26, 2, 'Steamroom'),
(27, 2, 'Ski'),
(28, 2, 'Library'),
(29, 2, 'Trekking'),
(30, 2, 'Horse riding'),
(31, 2, 'Newspapers'),
(32, 2, 'Ski-in/Ski-out'),
(33, 2, 'Fishing'),
(34, 2, 'Bicycles'),
(35, 2, 'Hamam'),
(36, 3, 'LCD/Plasma TV'),
(37, 3, 'Satellite TV'),
(38, 3, 'DVD Player'),
(39, 3, 'Fax machine'),
(40, 3, 'Telephone'),
(41, 3, 'Radio'),
(42, 3, 'Free Internet'),
(43, 3, 'Free Wi-Fi'),
(44, 3, 'Internet (extra charges)'),
(45, 3, 'Wi-Fi (extra charges)'),
(46, 4, 'Restaurant'),
(47, 4, 'Room service'),
(48, 4, 'Breakfast'),
(49, 4, 'Bar'),
(50, 4, 'Special diet menu'),
(51, 5, 'Free street parking'),
(52, 5, 'Free private parking'),
(53, 5, 'Paid private parking'),
(54, 5, 'Guarded parking'),
(55, 6, 'AC'),
(56, 6, 'View'),
(57, 6, 'Minibar'),
(58, 6, 'Room service'),
(59, 6, 'Fridge'),
(60, 6, 'Hair dryer'),
(61, 6, 'Fireplace'),
(62, 6, 'Heating'),
(63, 6, 'Safe'),
(64, 6, 'Coffee'),
(65, 6, 'Laundry'),
(66, 6, 'Dry cleaning'),
(67, 6, 'Ironing'),
(68, 6, 'Bridal suite'),
(69, 6, 'Non-smoking'),
(70, 6, 'Lift'),
(71, 6, 'Soundproof'),
(72, 6, 'Babysitting'),
(73, 6, 'Transport from/to airport'),
(74, 6, 'ATM'),
(75, 6, 'Daily cleaning service'),
(76, 6, 'Kitchen'),
(77, 6, 'Coffee machine'),
(78, 6, 'Kitchen appliances'),
(79, 6, 'Meeting rooms'),
(80, 6, 'Wake-up service'),
(81, 6, 'Water heater'),
(82, 6, 'Toaster'),
(83, 6, 'Personal care products'),
(84, 6, 'Heated pool'),
(85, 6, 'Indoor pool'),
(86, 6, 'Bathroom'),
(87, 6, 'Shared bathroom'),
(88, 7, 'Accessibility'),
(89, 7, 'Pets allowed'),
(90, 7, 'No pets allowed'),
(91, 7, 'Winter suggestion'),
(92, 7, 'Mini shuttle'),
(93, 7, 'Meeting tourism'),
(94, 7, 'Social tourism'),
(95, 7, 'Tourism for everyone'),
(96, 7, '24 hour reception'),
(97, 7, 'Baggage storage room'),
(98, 7, 'Safe'),
(99, 7, 'Smokers room'),
(100, 7, 'Fast check-in/out'),
(101, 7, 'Family rooms'),
(102, 7, 'No smoking'),
(103, 7, 'Private entrance'),
(104, 7, 'Fan'),
(105, 7, 'Lobby');

-- --------------------------------------------------------

--
-- Table structure for table `amenity_category`
--

CREATE TABLE IF NOT EXISTS `amenity_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `amenity_category`
--

INSERT INTO `amenity_category` (`id`, `name`) VALUES
(1, 'Exterior'),
(2, 'Activities'),
(3, 'Technology'),
(4, 'Food/Drinks'),
(5, 'Parking'),
(6, 'Services'),
(7, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `city_id`, `name`) VALUES
(1, 1, 'Marathopoli'),
(2, 2, 'Trikala');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefecture_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`prefecture_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `prefecture_id`, `name`) VALUES
(1, 1, 'Messinia'),
(2, 1, 'Korinthia');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'Greece');

-- --------------------------------------------------------

--
-- Table structure for table `prefecture`
--

CREATE TABLE IF NOT EXISTS `prefecture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `prefecture`
--

INSERT INTO `prefecture` (`id`, `country_id`, `name`) VALUES
(1, 1, 'Peloponnisos');

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE IF NOT EXISTS `property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `property_type_id` int(11) NOT NULL,
  `property_category_id` int(11) NOT NULL,
  `area_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `lon_lat` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `phone_alt` varchar(255) DEFAULT NULL,
  `phone_alt_2` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `site_url` varchar(255) DEFAULT NULL,
  `social_links` text,
  `description` text,
  `no_of_rooms` int(11) DEFAULT NULL,
  `last_edit` datetime NOT NULL,
  `edited_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_type_id` (`property_type_id`),
  KEY `property_category_id` (`property_category_id`),
  KEY `area_id` (`area_id`),
  KEY `user_id` (`user_id`),
  KEY `edited_by` (`edited_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `property_amenity`
--

CREATE TABLE IF NOT EXISTS `property_amenity` (
  `property_id` int(11) NOT NULL,
  `amenity_id` int(11) NOT NULL,
  PRIMARY KEY (`property_id`,`amenity_id`),
  KEY `amenity_id` (`amenity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `property_category`
--

CREATE TABLE IF NOT EXISTS `property_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `property_category`
--

INSERT INTO `property_category` (`id`, `type`, `value`) VALUES
(1, 'stars', 1),
(2, 'stars', 2),
(3, 'stars', 3),
(4, 'stars', 4),
(5, 'stars', 5);

-- --------------------------------------------------------

--
-- Table structure for table `property_extra`
--

CREATE TABLE IF NOT EXISTS `property_extra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_id` (`property_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `property_period`
--

CREATE TABLE IF NOT EXISTS `property_period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `from_month` varchar(255) NOT NULL,
  `to_month` varchar(255) NOT NULL,
  `from_date` int(11) NOT NULL,
  `to_date` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_id` (`property_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `property_seo`
--

CREATE TABLE IF NOT EXISTS `property_seo` (
  `property_id` int(11) NOT NULL,
  `keywords` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `property_type`
--

CREATE TABLE IF NOT EXISTS `property_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `property_type`
--

INSERT INTO `property_type` (`id`, `name`) VALUES
(1, 'Hotel'),
(2, 'Inn'),
(3, 'Rooms/Apartments'),
(4, 'Hostel'),
(5, 'Camping');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` datetime NOT NULL,
  `registered_on` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `discount_code` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `role`, `email`, `password`, `last_login`, `registered_on`, `name`, `phone`, `country_id`, `discount_code`) VALUES
(1, 16, 'info@subdee.org', 'asd', '2013-12-17 10:31:55', '2013-12-15 00:00:00', 'Kostas', '+31682009216', 1, '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `amenity`
--
ALTER TABLE `amenity`
  ADD CONSTRAINT `amenity_ibfk_1` FOREIGN KEY (`amenity_category_id`) REFERENCES `amenity_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `area`
--
ALTER TABLE `area`
  ADD CONSTRAINT `area_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`prefecture_id`) REFERENCES `prefecture` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `prefecture`
--
ALTER TABLE `prefecture`
  ADD CONSTRAINT `prefecture_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `property`
--
ALTER TABLE `property`
  ADD CONSTRAINT `property_ibfk_1` FOREIGN KEY (`property_type_id`) REFERENCES `property_type` (`id`),
  ADD CONSTRAINT `property_ibfk_2` FOREIGN KEY (`property_category_id`) REFERENCES `property_category` (`id`),
  ADD CONSTRAINT `property_ibfk_3` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`),
  ADD CONSTRAINT `property_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `property_ibfk_5` FOREIGN KEY (`edited_by`) REFERENCES `user` (`id`);

--
-- Constraints for table `property_amenity`
--
ALTER TABLE `property_amenity`
  ADD CONSTRAINT `property_amenity_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `property_amenity_ibfk_2` FOREIGN KEY (`amenity_id`) REFERENCES `amenity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `property_extra`
--
ALTER TABLE `property_extra`
  ADD CONSTRAINT `property_extra_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `property_period`
--
ALTER TABLE `property_period`
  ADD CONSTRAINT `property_period_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `property_seo`
--
ALTER TABLE `property_seo`
  ADD CONSTRAINT `property_seo_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
