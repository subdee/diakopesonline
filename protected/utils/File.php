<?php

namespace application\utils;
use Yii;
use CHtml;

/**
 * Class File
 * @package application\utils
 */
class File {

	public static function image($name, $print = true) {
		$path = Yii::app()->request->baseUrl . '/images/' . $name;
		if (!$print)
			return $path;
		echo CHtml::image($path);
	}

	public static function css($name) {
		return Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/' . $name . '.css');
	}

	public static function js($name) {
		return Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/' . $name . '.js');
	}

	public static function document($name) {
		return Yii::app()->request->baseUrl . '/documents/' . $name;
	}

	public static function upload($subdir) {
		return Yii::getPathOfAlias('uploads') . '/' . $subdir;
	}

	public static function uploads($subdir) {
		return Yii::app()->request->baseUrl . '/uploads/' . $subdir;
	}
}