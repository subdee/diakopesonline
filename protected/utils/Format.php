<?php

namespace application\utils;
use Yii;

/**
 * Class Format
 * @package application\utils
 */
class Format {

	public static function currency($value, $currency = '€') {
		return Yii::app()->numberFormatter->formatCurrency($value, $currency);
	}

	public static function datetime($date, $time = true) {
		if ($time)
			return Yii::app()->dateFormatter->formatDateTime(strtotime($date));
		else
			return Yii::app()->dateFormatter->formatDateTime(strtotime($date), 'medium', null);
	}

	public static function seo($string) {
		$string = strtolower($string);
		$string = preg_replace('/[^a-zA-Z0-9\s]/', '', $string);
		$string = preg_replace('{ +}', ' ', $string);
		$string = trim($string);
		$string = str_replace(' ', '-', $string);

		return $string;
	}

}