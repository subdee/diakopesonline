<?php

namespace application\utils;

use Yii;

/**
 * Class Url
 * @package application\utils
 */
class Url {

	public static function create($path, $params = array()) {
		return Yii::app()->createUrl($path, $params);
	}

	public static function createAbsolute($path, $params = array()) {
		return Yii::app()->createAbsoluteUrl($path, $params);
	}

}