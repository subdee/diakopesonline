<?php

namespace application\utils;
use Yii;

/**
 * Class Profile
 * @package application\utils
 */
class Profile {

	public static function isLoggedIn() {
		return !Yii::app()->user->isGuest;
	}

	/**
	 * @return User
	 */
	public static function get() {
		return self::isLoggedIn() ? Yii::app()->user->profile : false;
	}

	public static function session($key, $value = null, $unset = false) {
		if ($unset) {
			unset(Yii::app()->session[$key]);
			return true;
		}
		if ($value)
            return Yii::app()->session[$key] = $value;
		return isset(Yii::app()->session[$key]) ? Yii::app()->session[$key] : false;
	}

	public static function notice($key, $message = null) {
		if ($message)
			return Yii::app()->user->setFlash($key, $message);
		return Yii::app()->user->hasFlash($key) ? Yii::app()->user->getFlash($key) : '';
	}

}