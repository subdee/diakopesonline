<?php

namespace application\utils;
use Yii;
use CHtml;

/**
 * Class Module
 * @package application\utils
 */
class Module {

	private static $_instance;
	private $_module;

	private function __construct() {}

	public static function instance(\CWebModule $name) {
		if (!self::$_instance) {
			self::$_instance = new Module();
		}
		self::$_instance->_module = $name;
		return self::$_instance;
	}

	public function image($name, $print = true) {
		$path = $this->_publish('images/' . $name);
		if (!$print)
			return $path;
		echo CHtml::image($path);
	}

	public function css($name) {
		return Yii::app()->clientScript->registerCssFile($this->_publish('css/' . $name . '.css'));
	}

	private function _publish($dir) {
		return Yii::app()->assetManager->publish($this->_module->basePath . '/assets/' . $dir);
	}
}