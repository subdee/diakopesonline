<?php

namespace application\utils;
use Yii;
use CEvent;

/**
 * Class Event
 * @package application\utils
 */
class Event {

	public static function trigger($event, $sender, $params = array()) {
		Yii::app()->eventSystem->$event(new CEvent($sender, $params));
	}

	public static function callback($event, $subject, $callback) {
		Yii::app()->eventSystem->$event = array($subject, $callback);
	}
}