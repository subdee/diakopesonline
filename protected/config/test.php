<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
			'db' => array(
				'connectionString' => 'mysql:host=localhost;dbname=firm24-test',
				'emulatePrepare' => true,
				'username' => 'firm24',
				'password' => 'ETaQRhAjndP8vqx6',
				'charset' => 'utf8',
			),
			'urlManager' => array(
				'showScriptName' => true,
			),
		),
	)
);
