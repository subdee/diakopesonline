<?php
return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'DiakopesOnline',
	'defaultController' => 'dashboard',
	'preload' => array('log'),
	'language' => 'en',
	'import' => array(
		'application.models.*',
		'application.components.*',
		'bootstrap.helpers.TbHtml',
		'ext.yiimailer.YiiMailer',
	),
	'aliases' => array(
		'utils' => realpath(__DIR__ . '/../utils'),
		'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'),
		'html5editor' => realpath(__DIR__ . '/../extensions/html5editor'),
		'yiiwheels' => realpath(__DIR__ . '/../extensions/yiiwheels'),
		'icepay' => realpath(__DIR__ . '/../extensions/icepay'),
		'uploads' => realpath(__DIR__ . '/../../uploads'),
		'vendors' => realpath(__DIR__ . '/../vendors'),
	),
	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'asd',
			'ipFilters' => array('127.0.0.1', '::1'),
			'generatorPaths' => array(
				'bootstrap.gii',
			),
		),
		'lawyer',
		'manager',
		'notary',
		'admin',
	),

	'components' => array(
		'user' => array(
			'allowAutoLogin' => true,
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),
		'yiiwheels' => array(
			'class' => 'yiiwheels.YiiWheels',
		),
		'eventSystem' => array(
			'class' => 'EventSystem'
		),
		'ePdf' => array(
			'class' => 'ext.yii-pdf.EYiiPdf',
			'params' => array(
				'mpdf' => array(
					'librarySourcePath' => 'application.vendors.mpdf.*',
					'constants' => array(
						'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
					),
					'class' => 'mpdf',
				),
			),
		),
		'request' => array(
			'enableCsrfValidation' => true,
			'class' => 'HttpRequest',
			'noCsrfValidationRoutes' => array(
                'admin/default/upload'
			),
		),
		'urlManager' => array(
			'urlFormat' => 'path',
			'rules' => array(
				'<module:\w>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
			'showScriptName' => false,
		),
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=diakopesonline',
			'emulatePrepare' => true,
			'username' => 'diakopesonline',
			'password' => '2h9KeapyXuAq7p9h',
			'charset' => 'utf8',
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),

	'params' => array(),
);