<?php

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Firm 24',
	'preload'=>array('log'),
	'import' => array(
		'application.models.*',
		'application.components.*',
		'ext.yiimailer.YiiMailer',
	),
	'aliases' => array(
		'webroot' => realpath(__DIR__ . '/../../'),
		'uploads' => realpath(__DIR__ . '/../../uploads'),
	),
	'components'=>array(
		'ePdf' => array(
			'class' => 'ext.yii-pdf.EYiiPdf',
			'params' => array(
				'mpdf' => array(
					'librarySourcePath' => 'application.vendors.mpdf.*',
					'constants' => array(
						'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
					),
					'class' => 'mpdf',
				),
			),
		),
		'request' => array(
			'hostInfo' => 'http://localhost/firm24/',
            'baseUrl' => '/',
            'scriptUrl' => '',
        ),
		'urlManager' => array(
			'urlFormat' => 'path',
			'rules' => array(
				'<module:\w>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
			'showScriptName' => false,
		),
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=firm24',
			'emulatePrepare' => true,
			'username' => 'firm24',
			'password' => 'ETaQRhAjndP8vqx6',
			'charset' => 'utf8',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);