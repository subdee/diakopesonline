<?php use \application\utils\File as file; ?>
<?php use \application\utils\Url as url; ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta content="en-us" http-equiv="Content-Language">
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
	<title>{subject}</title>
	<style type="text/css">
		body {
			margin: 0;
			padding: 0;
			background-color: #eeeeee;
			color: #999999;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 12px;
			-webkit-text-size-adjust: none;
		}

		h1, h2, h3, h4, h5, h6 {
			color: #39434d !important;
			margin-bottom: 10px !important;
		}

		a, a:link, a:visited {
			color: #777777;
			text-decoration: none;
			border-bottom: 1px #777777 dotted;
		}

		a:hover, a:active {
			text-decoration: none;
			color: #0f79aa !important;
			border-bottom: 1px #0f79aa dotted !important;
		}

		img {
			border: 0;
		}

		/*Hotmail and Yahoo specific code*/
		.ReadMsgBody {
			width: 100%;
		}

		.ExternalClass {
			width: 100%;
		}

		.yshortcuts {
			color: #999999
		}

		.yshortcuts a span {
			color: #777777
		}

		/*Hotmail and Yahoo specific code*/
	</style>
</head>
<body link="#777777" vlink="#777777">
<table id="container" align="center" cellpadding="0" cellspacing="0"
	   style="width: 100%; margin:0; padding:0; background-color:#eeeeee;">
	<tr>
		<td style="padding:0 20px;">
			<table width="620" align="center" cellpadding="0" cellspacing="0"
				   style="border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-weight:normal; font-size:12px; line-height:15pt; color:#999999; margin:0 auto;">
				<tr>
					<td style="color:#ffffff; padding:20px 0;">
						<img align="left" border="0" vspace="0" hspace="0" alt="Logo"
							 src="<?php echo url::createAbsolute(file::image('logo-dark.png', false)); ?>">
					</td>
				</tr>
			</table>
			<table width="620" align="center" cellpadding="0" cellspacing="0"
				   style="border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-weight:normal; font-size:12px; line-height:15pt; color:#999999; margin:0 auto;">
				<tr>
					<td width="618" height="5" valign="top" bgcolor="#2a8fbd" style="font-size:2px; line-height:0px;">
						<table width="620" cellpadding="0" cellspacing="0"
							   style="border-collapse:collapse; border-spacing: 0; margin:0; padding:0; line-height:0px;">
							<tr>
								<td valign="top" height="5" width="5" style="font-size:2px; line-height:0px;"><img
										alt="" height="5" src="<?php echo url::createAbsolute(file::image('mail/borderTopLeft4.gif', false)); ?>" width="5"
										align="right" vspace="0" hspace="0" border="0" style="display:block;"></td>
								<td valign="top" height="5" width="610" style="font-size:2px; line-height:0px;"></td>
								<td valign="top" height="5" width="5" style="font-size:2px; line-height:0px;"><img
										alt="" height="5" src="<?php echo url::createAbsolute(file::image('mail/borderTopRight4.gif', false)); ?>" width="5"
										align="right" vspace="0" hspace="0" border="0" style="display:block;"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor="#2a8fbd"
						style="padding:1px 0 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:15pt; color:#999999;">
						<table width="580" align="center" cellpadding="0" cellspacing="0"
							   style="border-collapse:collapse; text-align:left; font-family:Arial, Helvetica, sans-serif; font-weight:normal; font-size:12px; line-height:15pt; color:#999999; ">
							<tr>
								<td valign="left" style="padding:10px 0 0 0; color:#ffffff !important;">
									<div
										style="padding:0; font-family:Arial, Helvetica, sans-serif; font-size:26px; line-height:24pt; font-weight:bold; margin:0; display:block; color:#ffffff;">
										<?php echo $data['title']; ?>
									</div>
									<span
										style="padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:17pt; font-weight:lighter; display:block;color:#ffffff;"> </span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="bottom" bgcolor="#2a8fbd" height="6"
						style="font-size:2px; line-height:0px; padding:0 0 0 39px;">
						<img alt="" height="6" src="<?php echo url::createAbsolute(file::image('mail/arrowUp.gif', false)); ?>" width="12" align="left" vspace="0"
							 hspace="0" border="0" style="display:block;"></td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF"
						style="padding:15px 20px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:15pt; color:#999999;">
						<?php echo $content; ?>

					</td>
				</tr>
				<tr>
					<td bgcolor="#f4f4f4"
						style="padding:17px 20px 12px 20px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:15pt; color:#999999; border-top:1px #eee dashed;">
						<table width="580" cellpadding="0" cellspacing="0"
							   style="border-collapse:collapse; border-spacing:0; border-width:0;">
							<tr>
								<td width="180" valign="top"
									style="padding:0 20px 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:15pt; color:#999999;">
									<table width="279" cellpadding="0" cellspacing="0"
										   style="border-collapse:collapse; border-spacing:0; border-width:0;">
										<tr>
											<td valign="top" width="28" style="padding:0 0 0 0; line-height:100%;"><img
													border="0" alt="Home:" height="12"
													src="<?php echo url::createAbsolute(file::image('mail/homeIcon.gif', false)); ?>" width="11"></td>
											<td width="243"
												style="padding:0 0 10px 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:100%; color:#999999;"
												valign="top">
												<a href="http://www.firm24.com"
												   style="border-bottom:1px #777777 dotted; text-decoration:none; color:#777777;">www.firm24.com</a>
											</td>
										</tr>
										<tr>
											<td valign="top" style="padding:2px 0 0 1px; line-height:100%;"><img
													border="0" alt="Phone:" height="10"
													src="<?php echo url::createAbsolute(file::image('mail/phoneIcon.gif', false)); ?>" width="7"></td>
											<td width="243"
												style="padding:0 0 10px 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:100%; color:#999999;"
												valign="top">
												<a href="#"
												   style="border-bottom:1px #777777 dotted; text-decoration:none; color:#777777;">Tel:
													+31 (0)20 30 80 675 </a></td>
										</tr>
										<tr>
											<td valign="top" style="padding:2px 0 0 1px; line-height:100%;"><img
													border="0" alt="Phone:" height="10"
													src="<?php echo url::createAbsolute(file::image('mail/phoneIcon.gif', false)); ?>" width="7"></td>
											<td width="243"
												style="padding:0 0 10px 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:100%; color:#999999;"
												valign="top">
												<a href="#"
												   style="border-bottom:1px #777777 dotted; text-decoration:none; color:#777777;">Tel:
													+31 (0)6 4659 3418 </a></td>
										</tr>
										<tr>
											<td valign="top" width="28" style="padding:2px 0 0 0; line-height:100%;">
												<img border="0" alt="Email:" height="9"
													 src="<?php echo url::createAbsolute(file::image('mail/emailIcon.gif', false)); ?>" width="12"></td>
											<td width="243"
												style="padding:0 0 10px 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:100%; color:#999999;"
												valign="top">
												<a href="mailto:support@firm24.com"
												   style="border-bottom:1px #777777 dotted; text-decoration:none; color:#777777;">support@firm24.com</a>
											</td>
										</tr>
									</table>
								</td>
								<td width="180" valign="top"
									style="padding:0 20px 0 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:15pt; color:#999999;">
									<table width="279" cellpadding="0" cellspacing="0"
										   style="border-collapse:collapse; border-spacing:0; border-width:0;">
										<tr>
											<td valign="top" width="28" style="padding:0 0 0 0; line-height:100%;">
												&nbsp;</td>
											<td width="243"
												style="padding:0 0 10px 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:100%; color:#999999;"
												valign="top">
												<a href="#"
												   style="border-bottom:1px #777777 dotted; text-decoration:none; color:#777777;">Firm24
													B.V. <br></a></td>
										</tr>
										<tr>
											<td valign="top" width="28" style="padding:0 0 0 0; line-height:100%;">
												&nbsp;</td>
											<td width="243"
												style="padding:0 0 10px 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:100%; color:#999999;"
												valign="top">
												<a href="#"
												   style="border-bottom:1px #777777 dotted; text-decoration:none; color:#777777;">Herikerbergweg
													284 <br></a></td>
										</tr>
										<tr>
											<td valign="top" width="28" style="padding:0 0 0 0; line-height:100%;">
												&nbsp;</td>
											<td width="243"
												style="padding:0 0 10px 0; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:100%; color:#999999;"
												valign="top">
												<a href="#"
												   style="border-bottom:1px #777777 dotted; text-decoration:none; color:#777777;">1101
													CT Amsterdam<br></a></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor="#F4F4F4" height="5" style="font-size:2px; line-height:0px;" valign="bottom">
						<table width="620" cellpadding="0" cellspacing="0"
							   style="border-collapse:collapse; border-spacing: 0; margin:0; padding:0; line-height:0px;">
							<tr>
								<td valign="top" height="5" width="5" style="font-size:2px; line-height:0px;"><img
										alt="" height="5" src="<?php echo url::createAbsolute(file::image('mail/borderBottomLeft2.gif', false)); ?>" width="5"
										align="right" vspace="0" hspace="0" border="0" style="display:block;"></td>
								<td valign="top" height="5" width="610" style="font-size:2px; line-height:0px;"></td>
								<td valign="top" height="5" width="5" style="font-size:2px; line-height:0px;"><img
										alt="" height="5" src="<?php echo url::createAbsolute(file::image('mail/borderBottomRight2.gif', false)); ?>" width="5"
										align="right" vspace="0" hspace="0" border="0" style="display:block;"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="20" style="font-size:2px; line-height:2px;"><img alt="" height="20"
																				 src="<?php echo url::createAbsolute(file::image('mail/shadow620.gif', false)); ?>"
																				 width="620" border="0"
																				 style="display:block;"></td>
				</tr>
			</table>
			<table width="620" align="center" cellpadding="0" cellspacing="0"
				   style="border-collapse:collapse; text-align:center; font-family:Arial, Helvetica, sans-serif; font-weight:normal; font-size:12px; line-height:15pt; color:#999999; margin:0 auto;">
				<tr>
					<td style="color:#999999; padding:0 20px 20px 20px; text-align:center;">
						Copyright &copy; 2013 Firm24
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>