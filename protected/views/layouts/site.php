<?php /* @var $this Controller */ ?>
<?php use \application\utils\File as file; ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="nl"/>

	<?php Yii::app()->bootstrap->register(); ?>

	<?php file::css('main'); ?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<?php echo $content; ?>
</body>
</html>
