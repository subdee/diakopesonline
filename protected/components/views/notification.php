<div>
	<?php if ($error) : ?>
		<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_ERROR, $error); ?>
	<?php endif; ?>
	<?php if ($success) : ?>
		<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_SUCCESS, $success); ?>
	<?php endif; ?>
	<?php if ($info) : ?>
		<?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, $info); ?>
	<?php endif; ?>
</div>