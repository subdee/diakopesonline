<?php

use \application\utils\Profile as profile;

class Notification extends CWidget {

	public $models = array();
	private $error;
	private $success;
	private $info;

	public function init() {
		$this->error = profile::notice('error');
		$this->success = profile::notice('success');
		$this->info = profile::notice('info');
	}

	public function run() {
		$this->render('notification', array(
			'error' => $this->error,
			'success' => $this->success,
			'info' => $this->info,
			'models' => $this->models
		));
	}

}