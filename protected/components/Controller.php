<?php

use \application\utils\Profile as profile;

class Controller extends CController {
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout = '//layouts/main';

	public function filterAccessControl($filterChain) {
		$filter = new AccessControlFilter;
		$filter->setRules($this->accessRules());
		$filter->filter($filterChain);
	}

}