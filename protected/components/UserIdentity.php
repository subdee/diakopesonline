<?php

class UserIdentity extends CUserIdentity {

	public $role = false;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate() {
		$user = User::model()->find('email = :username AND password = :password', array(
			':username' => $this->username,
			':password' => $this->password,
		));
		if (!$user) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} elseif ($this->role && $user->role != $this->role) {
			$this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
		} else {
			$user->last_login = date(DATE_ISO8601);
			$user->saveAttributes(array('last_login'));
			$this->setState('profile', $user);
			$this->errorCode = self::ERROR_NONE;
		}
		return !$this->errorCode;
	}
}