<?php
/**
 * Created by PhpStorm.
 * User: subdee
 * Date: 11/20/13
 * Time: 3:08 PM
 */

require __DIR__ . '/../extensions/bootstrap/widgets/TbButtonColumn.php';

class CustomButtonColumn extends TbButtonColumn {
	protected function renderButton($id, $button, $row, $data)
	{
		if (isset($button['visible']) && !$this->evaluateExpression(
				$button['visible'],
				array('row' => $row, 'data' => $data)
			)
		) {
			return;
		}

		$url = TbArray::popValue('url', $button, '#');
		if ($url !== '#') {
			$url = $this->evaluateExpression($url, array('data' => $data, 'row' => $row));
		}

		$imageUrl = TbArray::popValue('imageUrl', $button, false);
		$label = TbArray::popValue('label', $button, $id);
		$options = TbArray::popValue('options', $button, array());

		TbArray::defaultValue('title', $label, $options);
		TbArray::defaultValue('rel', 'tooltip', $options);

		if ($icon = TbArray::popValue('icon', $button, false)) {
			if ($text = TbArray::popValue('text', $button, false))
				echo CHtml::link(TbHtml::icon($icon) . ' ' . $text, $url, $options) . '<br>';
			else
				echo CHtml::link(TbHtml::icon($icon), $url, $options);
		} else {
			if ($imageUrl && is_string($imageUrl)) {
				echo CHtml::link(CHtml::image($imageUrl, $label), $url, $options);
			} else {
				echo CHtml::link($label, $url, $options);
			}
		}
	}
} 