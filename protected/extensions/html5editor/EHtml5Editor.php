<?php

class EHtml5Editor extends CInputWidget {
	/**
	 * Editor language
	 * Supports: de-DE, es-ES, fr-FR, pt-BR, sv-SE, it-IT
	 */
	public $lang = 'en';

	/**
	 * Html options that will be assigned to the text area
	 */
	public $htmlOptions = array();

	/**
	 * Editor options that will be passed to the editor
	 */
	public $editorOptions = array();

	/**
	 * Editor width
	 */
	public $width = '100%';

	/**
	 * Editor height
	 */
	public $height = '400px';

	/**
	 * Display editor
	 */
	public function run() {
		list($name, $id) = $this->resolveNameID();

		$this->registerClientScript($id);

		$this->htmlOptions['id'] = $id;

		if (!array_key_exists('style', $this->htmlOptions)) {
			$this->htmlOptions['style'] = "width:{$this->width};height:{$this->height};";
		}

		if ($this->hasModel()) {
			echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
		} else {
			echo CHtml::textArea($name, $this->value, $this->htmlOptions);
		}
	}

	/**
	 * Register required script files
	 *
	 * @param string $id
	 */
	public function registerClientScript($id) {
		$baseUrl = Yii::app()->assetManager->publish(Yii::getPathOfAlias('html5editor.assets'));

		Yii::app()->getClientScript()->registerScriptFile("{$baseUrl}/js/advanced.js", CClientScript::POS_END);
		Yii::app()->getClientScript()->registerScriptFile("{$baseUrl}/js/wysihtml5-0.3.0.js", CClientScript::POS_END);
		Yii::app()->getClientScript()->registerScriptFile("{$baseUrl}/js/bootstrap-wysihtml5.js", CClientScript::POS_END);
		Yii::app()->getClientScript()->registerCssFile("{$baseUrl}/css/bootstrap-wysihtml5.css");

		$this->normalizeStylesheetsProperty();
		$this->insertDefaultStylesheetIfColorsEnabled();

		$options = CJSON::encode($this->editorOptions);

		$script = array();
		/**
		 * The default stylesheet option is incompatible with yii paths so it is reset here.
		 * The insertDefaultStylesheetIfColorsEnabled includes the correct stylesheet if needed.
		 *
		 * Any other changes to defaults should be made here.
		 */
		$script[] = "$.fn.wysihtml5.defaultOptions.stylesheets = [];";

		/**
		 * Check if we need a deep copy for the configuration.
		 */
		if (isset($this->editorOptions['deepExtend']) && $this->editorOptions['deepExtend'] === true) {
			$script[] = "$('#{$id}').wysihtml5('deepExtend', {$options});";
		} else {
			$script[] = "$('#{$id}').wysihtml5({$options});";
		}

		Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $id, implode("\n", $script));
	}

	private function insertDefaultStylesheetIfColorsEnabled() {
		if (empty($this->editorOptions['color'])) {
			return;
		}

		$defaultStyleSheetUrl = Yii::app()->bootstrap->getAssetsUrl() . '/css/wysiwyg-color.css';
		array_unshift($this->editorOptions['stylesheets'], $defaultStyleSheetUrl); // we want default css to be first
	}

	private function normalizeStylesheetsProperty() {
		if (empty($this->editorOptions['stylesheets'])) {
			$this->editorOptions['stylesheets'] = array();
		} else if (is_array($this->editorOptions['stylesheets'])) {
			$this->editorOptions['stylesheets'] = array_filter(
				$this->editorOptions['stylesheets'],
				'is_string'
			);
		} else if (is_string($this->editorOptions['stylesheets'])) {
			$this->editorOptions['stylesheets'] = array($this->editorOptions['stylesheets']);
		} else // presumably if this option is neither an array or string then it's some erroneous value; clean it
		{
			$this->editorOptions['stylesheets'] = array();
		}
	}
}
