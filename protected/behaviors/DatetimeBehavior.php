<?php
/**
 * User: subdee
 * Date: 10/23/13
 * Time: 5:05 PM
 */

class DatetimeBehavior extends CActiveRecordBehavior {
	public function beforeSave($event) {
		foreach ($event->sender->tableSchema->columns as $columnName => $column) {

			if ($column->dbType != 'datetime' && $column->dbType != 'date')
				continue;

			if (!strlen($event->sender->$columnName)) {
				$event->sender->$columnName = null;
				continue;
			}

			$event->sender->$columnName = date(DATE_ISO8601, strtotime($event->sender->$columnName));
		}
	}

	public function afterFind($event) {
		foreach ($event->sender->tableSchema->columns as $columnName => $column) {
			if (($column->dbType != 'datetime' && $column->dbType != 'date') || $event->sender->$columnName == null)
				continue;

			$event->sender->$columnName = date('d-m-Y', strtotime($event->sender->$columnName));
		}
	}
} 