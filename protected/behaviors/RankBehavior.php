<?php
/**
 * Created by PhpStorm.
 * User: subdee
 * Date: 11/20/13
 * Time: 6:33 PM
 */

class RankBehavior extends CActiveRecordBehavior {
	public function setOrder() {
		if ($this->owner->isNewRecord) {
			$max = Yii::app()->db->createCommand()
				->select('MAX(order_list) as max')
				->from($this->owner->tableName())
				->where('dox_document_id = :doc')
				->bindValue(':doc', $this->owner->dox_document_id)
				->queryScalar();
			$this->owner->order_list = $max + 1;
		}
	}

	public function moveByPk($dir, $id) {
		if ($model = $this->owner->findByPk($id)) {
			switch ($dir) {
				case 'up':
					$this->owner->model()->updateCounters(array('order_list' => -1),
						'id = :id AND order_list > 1 AND dox_document_id = :doc',
						array(':id' => $id, ':doc' => $model->dox_document_id));
					$this->owner->model()->updateCounters(array('order_list' => 1),
						'order_list = :rank - 1 AND id != :id AND dox_document_id = :doc',
						array(':id' => $id, ':rank' => $model->order_list, ':doc' => $model->dox_document_id));
					return $model;
				case 'down':
					$this->owner->model()->updateCounters(array('order_list' => 1),
						'id = :id AND dox_document_id = :doc',
						array(':id' => $id, ':doc' => $model->dox_document_id));
					$this->owner->model()->updateCounters(array('order_list' => -1),
						'order_list = :rank + 1 AND order_list > 1 AND id != :id AND dox_document_id = :doc',
						array(':id' => $id, ':rank' => $model->order_list, ':doc' => $model->dox_document_id));
					return $model;
			}
		}
		return false;
	}
} 